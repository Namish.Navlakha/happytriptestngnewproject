package com.skillassure.happytrip.Pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.skillassure.happytrip.Util.Read_XLMX;

public class FlightSchedule {
	Logger log = LogManager.getLogger(FlightSchedule.class);
	WebDriver driver;
//	public Logger log;
	ExtentReports extent;
	ExtentHtmlReporter reporter;
	AdminLogin adminLogin = new AdminLogin();
	
	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
		// Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
		// Call getScreenshotAs method to create image file
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		// Move image file to new destination
		File DestFile = new File(fileWithPath);
		// Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
	}

	@BeforeSuite
	public void setup() {
		reporter = new ExtentHtmlReporter("./reports/rusult.html");
		reporter = new ExtentHtmlReporter("./test-output/emailable-report.html");
		extent = new ExtentReports();
		extent.attachReporter(reporter);
	}

	@BeforeTest
	public void setupTest() {

		System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
	}
	
//	@Test(dataProvider = "Login")
//	public void adminlogintest(String userName, String Password) throws IOException, InterruptedException {
//
//		ExtentTest logger = extent.createTest("TestAdmin_LogIn", "Login to the admin page");
//
//		log.debug("Login Start");
//
//		driver.get("http://43.254.161.195:8085/happytripcrclean1/");
//
//		driver.findElement(By.linkText("Log in as admin")).click();
//		
//		WebElement username1 = driver.findElement(By.id("username"));
//		username1.click();
//		username1.clear();
//		username1.sendKeys(userName);
//
//		WebElement password1 = driver.findElement(By.id("password"));
//		password1.click();
//		password1.clear();
//		password1.sendKeys(Password+"n");
//		
//		logger.log(Status.ERROR, "Admin LoggedIn");
//		logger.log(Status.ERROR, "Admin Try To Login With Wrong Credentials");
//		
//		driver.findElement(By.id("signInButton")).click();
////		Thread.sleep(3000);
//		
//		String pass = "adminn";
//		
//		Assert.assertEquals(Password, pass);
//		
//		
//		try {
//			FlightSchedule.takeSnapShot(driver,
//					"screenshots//scheduleTestLoginError.png");
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//		WebElement username = driver.findElement(By.id("username"));
//		username.click();
//		username.clear();
//		username.sendKeys(userName);
//
//		WebElement password = driver.findElement(By.id("password"));
//		password.click();
//		password.clear();
//		password.sendKeys(Password);
//
//		driver.findElement(By.id("signInButton")).click();
//
//		logger.log(Status.PASS, "Admin LoggedIn");
//		logger.log(Status.INFO, "Admin Login Success");
//
//		extent.flush();
//
//	}
	
	@Test(dataProvider = "Login")
	public void adminlogintesterror(String userName, String Password) throws IOException, InterruptedException {

		ExtentTest logger = extent.createTest("TestAdmin_LogIn", "Login to the admin page");

		log.debug("Login Start");

		driver.get("http://43.254.161.195:8085/happytripcrclean1/");

		driver.findElement(By.linkText("Log in as admin")).click();
			
		WebElement username = driver.findElement(By.id("username"));
		username.click();
		username.clear();
		username.sendKeys(userName);

		WebElement password = driver.findElement(By.id("password"));
		password.click();
		password.clear();
		password.sendKeys(Password);

		driver.findElement(By.id("signInButton")).click();

		logger.log(Status.PASS, "Admin LoggedIn");
		logger.log(Status.INFO, "Admin Login Success");

		extent.flush();

	}

	@Test(description = "Schedule Flight")
	public void scheduleFlight01() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");
		
		// Navigate to Schedule flights
		log.debug("Test Case 01 :- Navigate To Schedule Flights");
		driver.findElement(By.linkText(("Schedule Flight"))).click();
		logger1.log(Status.PASS, "Schedule Flight Navigation ");
		logger1.log(Status.INFO, "Clicked to Schedule Flight Successfully");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Choose Flights")
	public void scheduleFlight02() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Choose Flights
		log.debug("Test Case 02 :- Choose Flights");
		driver.findElement(By.xpath("//*[@id=\"flight\"]/option[24]")).click();
		driver.findElement(By.xpath("//*[@id=\"flight\"]/option[24]")).isSelected();
		logger1.log(Status.PASS, " Flight Been Choosed");
		logger1.log(Status.INFO, " Selected Flight Successfully ");
		Thread.sleep(1000);

		extent.flush();
	}

	@Test(description = "Choose Routes")
	public void scheduleFlight03() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Choose Routes
		log.debug("Test Case 03 :- Choose Routes");
		Select Route = new Select(driver.findElement(By.id("route")));
		Route.selectByIndex(12);
		logger1.log(Status.PASS, " Flight Selected from Dropdown");
		logger1.log(Status.INFO, " Selected Flight Successfully");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Apends '-2000' into distance")
	public void scheduleFlight04() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Distance
		log.error("Test Case 04 :- Apends -2000 into distance");
		WebElement distance1 = driver.findElement(By.id("distance"));
		distance1.clear();
		distance1.sendKeys("-2000");
		logger1.log(Status.ERROR, " Enter the distance to be Travelled");
		logger1.log(Status.ERROR, "provide valid Distance it must be in greater than 0");
		driver.findElement(By.id("signInButton")).click();
		Thread.sleep(3000);
		
		try {
			FlightSchedule.takeSnapShot(driver,
					"screenshots//scheduleTest4.png");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
		    Assert.assertEquals("100", "-2000");
		} catch(Throwable t) {              
		    org.testng.Assert.fail("provide valid Distance it must be in greater than 0");      
		}		
		
		extent.flush();
	}
	
	@Test(description = "Apends '00' into distance")
	public void scheduleFlight05() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Distance
		log.error("Test Case 05 :- Apends 00 into distance");
		WebElement distance2 = driver.findElement(By.id("distance"));
		distance2.clear();
		distance2.sendKeys("00");
		logger1.log(Status.ERROR, " Enter the distance to be Travelled");
		logger1.log(Status.ERROR, "provide valid Distance it must be in greater than 0");
		driver.findElement(By.id("signInButton")).click();
		Thread.sleep(3000);

		try {
			FlightSchedule.takeSnapShot(driver,
					"screenshots//scheduleTest5.png");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
		    Assert.assertEquals("100", "00");
		} catch(Throwable t) {              
		    org.testng.Assert.fail("provide valid Distance it must be in greater than 0");      
		}
		
		extent.flush();
	}
	
	@Test(description = "Apends '20000' into distance")
	public void scheduleFlight06() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Distance
		log.debug("Test Case 06 :- Apends 20000 into distance");
		WebElement distance = driver.findElement(By.id("distance"));
		distance.clear();
		distance.sendKeys("20000");
		logger1.log(Status.PASS, " Enter the distance to be Travelled");
		logger1.log(Status.INFO, "Distance Added Successfully ");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Select Departure Date")
	public void scheduleFlight07() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Departure Date
		log.debug("Test Case 07 :- Select Departure Date");
		driver.findElement(By.id("departureDate")).click();
		driver.findElement(By.xpath("//*[@id=\"AddSchedule\"]/dl/dd[6]/img")).click();
		driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[4]/td[4]/a")).click();
		logger1.log(Status.PASS, " Enter The Departure Date");
		logger1.log(Status.INFO, " departure Date added Successfully ");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Departure Time")
	public void scheduleFlight08() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// departure Time
		log.debug("Test Case 08 :- Departure Time");
		Select DepartureTime = new Select(driver.findElement(By.id("departureTime")));
		DepartureTime.selectByIndex(5);
		logger1.log(Status.PASS, " Departure Time Select from the Dropdown");
		logger1.log(Status.INFO, "Departure Time Successfull");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Arrival Date")
	public void scheduleFlight09() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Arrival Date
		log.debug("Test Case 09 :- Arrival Date");
		driver.findElement(By.id("arrivalDate")).click();
		driver.findElement(By.xpath("//*[@id=\"AddSchedule\"]/dl/dd[8]/img")).click();
		driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[5]")).click();
		logger1.log(Status.PASS, " Enter The Arrival Date");
		logger1.log(Status.INFO, " Arrival Date added Successfully");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Arrival Time")
	public void scheduleFlightA() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Arrival Time
		log.debug("Test Case 10 :- Arrival Time");
		Select ArrivalTime = new Select(driver.findElement(By.id("arrivalTime")));
		ArrivalTime.selectByIndex(10);
		logger1.log(Status.PASS, " Arrival Time Selected from the Dropdown");
		logger1.log(Status.INFO, " Arrival Time Successfull ");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Enter the '-100' Economy Cost")
	public void scheduleFlightB() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Test Case 9 : Enter the Economy cost
		log.error("Test Case 11 :- Enter the -100 Economy Cost");
		WebElement cost1 = driver.findElement(By.id("classEconomy"));
		cost1.clear();
		cost1.sendKeys("-100");
		logger1.log(Status.ERROR, " Enter the Economy Cost ");
		logger1.log(Status.ERROR, " provided Cost must in greater than 0 ");
		driver.findElement(By.id("signInButton")).click();
		Thread.sleep(3000);
		
		try {
			FlightSchedule.takeSnapShot(driver,
					"screenshots//scheduleTest11.png");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
		    Assert.assertEquals("100", "-100");
		} catch(Throwable t) {              
		    org.testng.Assert.fail("provided Cost must in greater than 0");      
		}
		
		extent.flush();
	}
	
	@Test(description = "Enter the '00' Economy Cost")
	public void scheduleFlightC() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Test Case 9 : Enter the Economy cost
		log.error("Test Case 12 :- Enter the 00 Economy Cost");
		WebElement cost2 = driver.findElement(By.id("classEconomy"));
		cost2.clear();
		cost2.sendKeys("00");
		logger1.log(Status.ERROR, " Enter the Economy Cost ");
		logger1.log(Status.ERROR, " provided Cost must in greater than 0");
		driver.findElement(By.id("signInButton")).click();
		Thread.sleep(3000);
	
		try {
			FlightSchedule.takeSnapShot(driver,
					"screenshots//scheduleTest12.png");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
		    Assert.assertEquals("100", "00");
		} catch(Throwable t) {              
		    org.testng.Assert.fail("provided Cost must in greater than 0");      
		}	
		
		extent.flush();
	}
	
	@Test(description = "Enter the 4500 Economy Cost")
	public void scheduleFlightD() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Test Case 9 : Enter the Economy cost
		log.debug("Test Case 13 :- Enter the Economy Cost");
		WebElement cost = driver.findElement(By.id("classEconomy"));
		cost.clear();
		cost.sendKeys("4500");
		logger1.log(Status.PASS, " Enter the Economy Cost ");
		logger1.log(Status.INFO, " Cost added Successfully ");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@Test(description = "Click Add Button")
	public void scheduleFlightE() throws InterruptedException {

		ExtentTest logger1 = extent.createTest("Schedule Flight", "Test Schedule Flight Page");

		// Test Case10: Clicking Add Button
		log.debug("Test Case 14 :- Click Add Button");
		driver.findElement(By.id("signInButton")).click();
		logger1.log(Status.PASS, "Clicking On Add Button");
		logger1.log(Status.INFO, " Add Button Clicked Successfully");
		Thread.sleep(1000);

		extent.flush();
	}
	
	@DataProvider(name = "Login")
	public static Object[][] testData() throws IOException {
		return Read_XLMX.testData();
	}

	@AfterSuite
	public void tearDown() {
		extent.flush();
	}
}
