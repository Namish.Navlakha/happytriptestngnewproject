package com.skillassure.happytrip.Pages;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.skillassure.happytrip.Util.Read_XLMX;

public class AdminLogin {
	WebDriver driver;
	public Logger log;
	ExtentReports extent;
	ExtentHtmlReporter reporter;
	Read_XLMX read_XLMX = new Read_XLMX();

	@BeforeSuite
	public void setup() {
		reporter = new ExtentHtmlReporter("test-output/emailable-report.html");
		extent = new ExtentReports();
		extent.attachReporter(reporter);
	}

	@BeforeTest
	public void setupTest() {
		System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
	}

	@Test(dataProvider = "Login")
	public void adminlogintest(String userName, String Password) throws IOException {

		ExtentTest logger = extent.createTest("TestAdmin_LogIn", "Login to the admin page");

		driver.get("http://43.254.161.195:8085/happytripcrclean1/");

		driver.findElement(By.linkText("Log in as admin")).click();

		WebElement username = driver.findElement(By.id("username"));
		username.click();
		username.clear();
		username.sendKeys(userName);

		WebElement password = driver.findElement(By.id("password"));
		password.click();
		password.clear();
		password.sendKeys(Password);

		driver.findElement(By.id("signInButton")).click();

		logger.log(Status.PASS, "Admin LoggedIn");
		logger.log(Status.INFO, "Admin Login Success");

//		extent.flush();

	}

	@AfterSuite
	public void tearDown() {
		extent.flush();
	}

	@DataProvider(name = "Login")
	public static Object[][] testData() throws IOException {
		return Read_XLMX.testData();
	}
}
